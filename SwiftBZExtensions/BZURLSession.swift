//
//  BZURLSession.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 19.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import Foundation

class BZURLSession: NSObject, URLSessionDelegate
{
    //MARK: Constants (Public)
    
    //MARK: Constants (Private)
    
    //MARK: Property (Public)
    
    public var theTimeoutInterval: Double = 30
    
    //MARK: Property (Private)
    
    private var theMainDataTask: URLSessionDataTask? = nil
    private var theMainURLSession: URLSession? = nil
    private var theData: Data? = nil //TODO: check if needed
    private var theLockObject: NSObject? = nil
    private var theCompletitionBlockWithData:((_: Data, _: Error?) -> Void)? = nil
    private var theCompletitionBlock:((_: Error?) -> Void)? = nil
    private var theProgressBlock:((_: Double) -> Void)? = nil
    private var theProgressBlockWithData:((_: Double, _: Data) -> Void)? = nil
    private var theLoadedProgress: Double? = nil
    private var isFinished: Bool = false
    
    //MARK: Class property (Public)
    
    static var theSessionIdentifier:Int! = 0
    
    //MARK: Class property (Private)
    
    //MARK: Class Methods (Public)
    
    //MARK: Class Methods (Private)
    
    //MARK: Init & Dealloc
    
    override init()
    {
        super.init()
        theLockObject = NSObject()
    }
    
    //MARK: Notifications
    
    //MARK: Delegates(URLSessionDelegate)

    //MARK: Methods(Public)
    
    public func methodStop()
    {
        self.theMainURLSession?.invalidateAndCancel()
    }
    
    public func methodStartGetTask(theURL: URL!, theCompletitionBlock:@escaping ((_: Data?, _: Error?) -> Void))
    {
        methodStop()
        self.theMainURLSession = URLSession(configuration: URLSessionConfiguration.default)

        let urlRequest = URLRequest(url: theURL, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: self.theTimeoutInterval)

        // make the request
        self.theMainDataTask = self.theMainURLSession?.dataTask(with: urlRequest, completionHandler:
        { (newData, newURLResponse, newError) in
            theCompletitionBlock(newData, newError)
        })
        self.theMainDataTask?.resume()
    }
    
    public func methodStartPostTask(theURL: URL!, thePostData: Data!,  theCompletitionBlock:@escaping ((_: Data?, _: Error?) -> Void))
    {
        methodStop()
        self.theMainURLSession = URLSession(configuration: URLSessionConfiguration.default)
        
        var theUrlRequest = URLRequest(url: theURL, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: self.theTimeoutInterval)

        theUrlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        theUrlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        theUrlRequest.httpMethod = "POST"
        // make the request
        self.theMainDataTask = self.theMainURLSession?.dataTask(with: theUrlRequest , completionHandler:
            { (newData, newURLResponse, newError) in
                theCompletitionBlock(newData, newError)
        })
        self.theMainDataTask?.resume()
    }
    
    //MARK: Methods(Private)
    
    //MARK: Standart methods

}
