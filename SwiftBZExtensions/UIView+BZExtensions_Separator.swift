//
//  UIView+BZExtensions_Separator.swift
//  SwiftBZExtensions
//
//  Created by User on 17.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit

enum BZViewSeparatorType {
    case None
    case Top
    case Bottom
    case Left
    case Right
}

extension UIView
{
    private struct AssociatedKeys {
        static var theRightSeparatorViewKey = "theRightSeparatorViewKey"
        static var theBottomSeparatorViewKey = "theBottomSeparatorViewKey"
        static var theTopSeparatorViewKey = "theTopSeparatorViewKey"
        static var theLeftSeparatorViewKey = "theLeftSeparatorViewKey"
        static var theSeparatorTypeKey = "theSeparatorTypeKey"
    }
    
    private var theBZViewSeparatorType: BZViewSeparatorType
    {
        get
        {
            var theType = objc_getAssociatedObject(self, &AssociatedKeys.theSeparatorTypeKey) as? BZViewSeparatorType
            if theType == nil
            {
                theType = BZViewSeparatorType.None
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theSeparatorTypeKey,
                    theType!,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
            return theType!
        }
        set
        {
                if newValue != self.theBZViewSeparatorType {
                    objc_setAssociatedObject(
                        self,
                        &AssociatedKeys.theSeparatorTypeKey,
                        newValue as BZViewSeparatorType,
                        .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                    )
                }
        }
    }
    
    public var theSeparatorsArray : Array<UIView>
    {
        get
        {
            var theArray = Array<UIView>()
            if let theBottomSeparatorView = objc_getAssociatedObject(self, &AssociatedKeys.theBottomSeparatorViewKey) as? UIView
            {
               theArray.append(theBottomSeparatorView)
            }
            if let theTopSeparatorView = objc_getAssociatedObject(self, &AssociatedKeys.theTopSeparatorViewKey) as? UIView
            {
                theArray.append(theTopSeparatorView)
            }
            if let theLeftSeparatorView = objc_getAssociatedObject(self, &AssociatedKeys.theLeftSeparatorViewKey) as? UIView
            {
                theArray.append(theLeftSeparatorView)
            }
            if let theRightSeparatorView = objc_getAssociatedObject(self, &AssociatedKeys.theRightSeparatorViewKey) as? UIView
            {
                theArray.append(theRightSeparatorView)
            }
            return theArray
        }
    }
    
    public var theTopSeparatorView: UIView
    {
        get
        {
            var theView = objc_getAssociatedObject(self, &AssociatedKeys.theTopSeparatorViewKey) as? UIView
            if theView == nil
            {
                let theTopSeparatorView = UIView()
                theView = theTopSeparatorView
                self.addSubview(theView!)
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theTopSeparatorViewKey,
                    theView!,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
                theView!.theHeight = 1
                theView!.theWidth = (theView!.superview?.theWidth)!
                theView!.theBZViewSeparatorType = BZViewSeparatorType.Top
            }
            return theView!
        }
    }
    
    public var theBottomSeparatorView: UIView
    {
        get
        {
            var theView = objc_getAssociatedObject(self, &AssociatedKeys.theBottomSeparatorViewKey) as? UIView
            if theView == nil
            {
                let theTopSeparatorView = UIView()
                theView = theTopSeparatorView
                self.addSubview(theView!)
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theBottomSeparatorViewKey,
                    theView!,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
                theView!.theHeight = 1
                theView!.theWidth = (theView!.superview?.theWidth)!
                theView!.theMaxY = (theView!.superview?.theHeight)!
                theView!.theBZViewSeparatorType = BZViewSeparatorType.Bottom
            }
            return theView!
        }
    }
    
    public var theLeftSeparatorView: UIView
    {
        get
        {
            var theView = objc_getAssociatedObject(self, &AssociatedKeys.theLeftSeparatorViewKey) as? UIView
            if theView == nil
            {
                let theLeftSeparatorView = UIView()
                theView = theLeftSeparatorView
                self.addSubview(theView!)
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theLeftSeparatorViewKey,
                    theView!,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
                theView!.theHeight = (theView!.superview?.theHeight)!
                theView!.theWidth = 1
                theView!.theBZViewSeparatorType = BZViewSeparatorType.Left
            }
            return theView!
        }
    }
    
    public var theRightSeparatorView: UIView
     {
        get
        {
            var theView = objc_getAssociatedObject(self, &AssociatedKeys.theRightSeparatorViewKey) as? UIView
            if theView == nil
            {
                let theRightSeparatorView = UIView()
                theView = theRightSeparatorView
                self.addSubview(theView!)
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theRightSeparatorViewKey,
                    theView!,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
                theView!.theHeight = (theView!.superview?.theHeight)!
                theView!.theWidth = 1
                theView!.theBZViewSeparatorType = BZViewSeparatorType.Right
                theView!.theMaxY = (theView!.superview?.theMaxY)!
            }
            return theView!
        }
    }

    
    private func methodAdjustSeparatorsViewArraySize(newWidth:Double)
    {
        let theSeparatorsViewArray = self.theSeparatorsArray
        if theSeparatorsViewArray.count == 0
        {
            return
        }
        for theCurrentView in theSeparatorsViewArray
        {
            switch theCurrentView.theBZViewSeparatorType
            {
            case .None: break
            case .Bottom:
                theCurrentView.theWidth = Double(theCurrentView.frame.size.width) * newWidth / Double((theCurrentView.superview?.frame.size.width)!)
            case .Top:
                theCurrentView.theWidth = Double(theCurrentView.frame.size.width) * newWidth / Double((theCurrentView.superview?.frame.size.width)!)
            default:break
            }
        }
    }
    private func methodAdjustSeparatorsViewArraySize(newHeight:Double)
    {
        let theSeparatorsViewArray = self.theSeparatorsArray
        if theSeparatorsViewArray.count == 0
        {
            return
        }
        for theCurrentView in theSeparatorsViewArray
        {
            switch theCurrentView.theBZViewSeparatorType
            {
            case .None: break
            case .Right:
                theCurrentView.theHeight = Double(theCurrentView.frame.size.height) * newHeight / Double((theCurrentView.superview?.frame.size.height)!)
            case .Left:
                theCurrentView.theHeight = Double(theCurrentView.frame.size.height) * newHeight / Double((theCurrentView.superview?.frame.size.height)!)
            default:break
            }
        }
    }
    private func methodAdjustSeparatorsViewArrayOrigin(newHeight:Double)
    {
        let theSeparatorsViewArray = self.theSeparatorsArray
        if theSeparatorsViewArray.count == 0
        {
            return
        }
        for theCurrentView in theSeparatorsViewArray
        {
            switch theCurrentView.theBZViewSeparatorType
            {
            case .None: break
            case .Bottom:
                theCurrentView.theMaxY = newHeight
            case .Right:
                theCurrentView.theCenterY = newHeight / 2
            case .Left:
                theCurrentView.theCenterY = newHeight / 2
            default:break
            }
        }
    }
    private func methodAdjustSeparatorsViewArrayOrigin(newWidth:Double)
    {
        let theSeparatorsViewArray = self.theSeparatorsArray
        if theSeparatorsViewArray.count == 0
        {
            return
        }
        for theCurrentView in theSeparatorsViewArray
        {
            switch theCurrentView.theBZViewSeparatorType
            {
            case .None: break
            case .Bottom:
                theCurrentView.theCenterX = newWidth / 2
            case .Top:
                theCurrentView.theCenterX = newWidth / 2
            case .Right:
                theCurrentView.theMaxX = newWidth
            default:break
            }
        }
    }
    
    open override class func initialize() {
        
        
        // make sure this isn't a subclass
        if self !== UIView.self {
            return
        }
        
        //            // Accessor
        //            method_exchangeImplementations(
        //                class_getInstanceMethod(self, Selector("swizzledBZ_frame")),
        //                class_getInstanceMethod(self, Selector("frame"))
        //            )
        // One-param setter
        method_exchangeImplementations(
            class_getInstanceMethod(self, #selector(UIView.swizzledBZSetFrame(frame:))),
            class_getInstanceMethod(self, #selector(setter: UIView.frame)))
    }
    
    // MARK: - Method Swizzling
    func swizzledBZSetFrame(frame: CGRect)
    {
        var theNewFrame = frame
        methodAdjustSeparatorsViewArraySize(newHeight: Double(frame.size.height))
        methodAdjustSeparatorsViewArraySize(newWidth: Double(frame.size.width))
        switch self.theBZViewSeparatorType
        {
        case .Top:
            theNewFrame.origin.y = 0
            theNewFrame.origin.x = CGFloat(Double((superview?.frame.size.width)!) / 2 - Double(theNewFrame.size.width) / 2)
        case .Bottom:
            theNewFrame.origin.y = (superview?.frame.size.height)! - theNewFrame.size.height
            theNewFrame.origin.x = (superview?.frame.size.width)! / 2 - theNewFrame.size.width / 2
        case .Left:
            theNewFrame.origin.y = (superview?.frame.size.height)! / 2 -  theNewFrame.size.height/2;
            theNewFrame.origin.x = 0;
        case .Right:
            theNewFrame.origin.y = (superview?.frame.size.height)! / 2 - theNewFrame.size.height / 2
            theNewFrame.origin.x = (superview?.frame.size.width)!  - theNewFrame.size.width
        default: break
        }
        swizzledBZSetFrame(frame: theNewFrame)
        methodAdjustSeparatorsViewArrayOrigin(newHeight: Double(theNewFrame.size.height))
        methodAdjustSeparatorsViewArrayOrigin(newWidth: Double(theNewFrame.size.width))
    }
}






























