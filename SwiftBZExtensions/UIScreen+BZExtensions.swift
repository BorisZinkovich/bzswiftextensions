//
//  UIScreen+BZExtensions.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 21.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit

extension UIScreen
{
    //MARK: Constants (Public)
    
    //MARK: Constants (Private)
    
    //MARK: Property (Public)
    
    //MARK: Property (Private)
    
    //MARK: Class property (Public)
    
    //MARK: Class property (Private)
    
    private static var isRetina: Bool! = UIScreen.main.scale == 2
    
    //MARK: Class Methods (Public)
    
    //MARK: Class Methods (Private)
    
    //MARK: Init & Dealloc
    
    //MARK: Notifications
    
    //MARK: Delegates()
    
    //MARK: Methods(Public)
    
    func isRetinaDisplay() -> Bool!
    {
        return UIScreen.isRetina
    }
 
    //MARK: Methods(Private)
    
    //MARK: Standart methods
}






























