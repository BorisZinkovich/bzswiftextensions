//
//  UnionFind.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 26.05.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import Foundation

class UnionFindManager <T: Hashable>
{
    private var theDataArray = [T: Int]()
    private var theSizeArray =  Array<Int>()
    private var theIdsArray = Array<Int> ()
    init()
    {
        
    }
    init(theArray: Array<T>)
    {
        for theItem in theArray
        {
            self.addSetWith(theItem)
        }
    }
    
    public func addSetWith(_ element: T)
    {
        theDataArray[element] = theIdsArray.count
        theIdsArray.append(theIdsArray.count)
        theSizeArray.append(1)
    }
    
    private func setByIndex(index: Int) -> Int?
    {
        if theIdsArray[index] == index
        {
            return index
        }
        else
        {
            return setByIndex(index: theIdsArray[index])
        }
    }
    
    public func setOfElement(element: T) -> Int?
    {
        if let indexOfElement = theDataArray[element]
        {
            return setByIndex(index: indexOfElement)
        }
        else
        {
            return nil
        }
    }
    
    func methodUnion(first:T, second:T)
    {
        if let theFirstSetIndex = setOfElement(element: first), let theSecondSetIndex = setOfElement(element: second)
        {
            if theFirstSetIndex != theSecondSetIndex
            {
                if theSizeArray[theFirstSetIndex]  < theSizeArray[theSecondSetIndex]
                {
                    theSizeArray[theSecondSetIndex] += theSizeArray[theFirstSetIndex]
                    theIdsArray[theFirstSetIndex] = theSecondSetIndex
                }
                else
                {
                    theSizeArray[theFirstSetIndex] += theSizeArray[theSecondSetIndex]
                    theIdsArray[theSecondSetIndex] = theFirstSetIndex
                }
            }
        }
    }
    
    func methodFind(first:T, second:T) -> Bool
    {
        if let theFirstSetIndex = setOfElement(element: first), let theSecondSetIndex = setOfElement(element: second)
        {
            return theFirstSetIndex == theSecondSetIndex
        }
        return false;
    }
}


























