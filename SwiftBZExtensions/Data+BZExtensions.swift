//
//  Data+BZExtensions.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 20.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import Foundation

extension Data {
    func hexEncodedString() -> String
    {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
