//
//  View+BZExtensions.swift
//  SwiftBZExtensions
//
//  Created by User on 17.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit

extension UIView
{
    var theMinX: Double
    {
        get
        {
            return Double(self.frame.origin.x)
        }
        set
        {
                var theFrameRect = self.frame;
                theFrameRect.origin.x = CGFloat(newValue);
                self.frame = theFrameRect;
        }
    }
    var theMinY: Double
        {
        get
        {
            return Double(self.frame.origin.y)
        }
        set
        {
                var theFrameRect = self.frame;
                theFrameRect.origin.y = CGFloat(newValue);
                self.frame = theFrameRect;
        }
    }
    var theCenterX: Double
    {
        get
        {
            let theFrameRect = self.frame;
            return Double(theFrameRect.origin.x) +  Double(theFrameRect.size.width) / 2;
        }
        set
        {
            var theFrameRect = self.frame;
            theFrameRect.origin.x = CGFloat(Double(newValue) - Double(theFrameRect.size.width) / 2);
            self.frame = theFrameRect;
        }
    }
    var theCenterY: Double
    {
        get
        {
            let theFrameRect = self.frame;
            return Double(theFrameRect.origin.y) +  Double(theFrameRect.size.height) / 2;
        }
        set
        {
            var theFrameRect = self.frame;
            theFrameRect.origin.y = CGFloat(Double(newValue) - Double(theFrameRect.size.height) / 2);
            self.frame = theFrameRect;
        }
    }
    var theMaxX: Double
    {
        get
        {
            let theFrameRect = self.frame;
            return Double(theFrameRect.origin.x) +  Double(theFrameRect.size.width);
        }
        set
        {
                var theFrameRect = self.frame;
                theFrameRect.origin.x = CGFloat(Double(newValue) - Double(theFrameRect.size.width));
                self.frame = theFrameRect;
        }
    }
    var theMaxY: Double
    {
        get
        {
            let theFrameRect = self.frame;
            return Double(theFrameRect.origin.y) +  Double(theFrameRect.size.height);
        }
        set
        {
            var theFrameRect = self.frame;
            theFrameRect.origin.y = CGFloat(Double(newValue) - Double(theFrameRect.size.height));
            self.frame = theFrameRect;
        }
    }
    var theWidth: Double
    {
       get
       {
            return Double(self.frame.size.width)
        }
        set
        {
            assert(newValue >= 0)
            var theFrameRect = self.frame;
            theFrameRect.size.width = CGFloat(newValue);
            self.frame = theFrameRect;
        }
    }
    var theHeight: Double
    {
        get
        {
            return Double(self.frame.size.height)
        }
        set
        {
            assert(newValue >= 0)
            var theFrameRect = self.frame;
            theFrameRect.size.height = CGFloat(newValue);
            self.frame = theFrameRect;
        }
    }

}





























