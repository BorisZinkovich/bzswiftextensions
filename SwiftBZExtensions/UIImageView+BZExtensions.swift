//
//  UIImageView+BZExtensions.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 21.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit

extension UIImageView
{
    //MARK: Constants (Public)
    
    //MARK: Constants (Private)
    
    //MARK: Property (Public)
    
    //MARK: Property (Private)
    
    //MARK: Class property (Public)
    
    //MARK: Class property (Private)
    
    //MARK: Class Methods (Public)
    
    //MARK: Class Methods (Private)
    
    //MARK: Init & Dealloc
    
    //MARK: Notifications
    
    //MARK: Delegates()
    
    //MARK: Methods(Public)
    
    public func methodSetImage(theImage: UIImage!, theCornerRadius: Double)
    {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 1.0);
        
        // Add a clip before drawing anything, in the shape of an rounded rect
        let theBeizerPath = UIBezierPath.init(roundedRect: self.bounds, cornerRadius: CGFloat(theCornerRadius))
        theBeizerPath.addClip()
        // Draw your image
        theImage.draw(in: self.bounds)
        // Get the image, here setting the UIImageView image
        self.image = UIGraphicsGetImageFromCurrentImageContext();
        
        // Lets forget about that we were drawing
        UIGraphicsEndImageContext();

    }
    
    //MARK: Methods(Private)
    
    //MARK: Standart methods
}




























