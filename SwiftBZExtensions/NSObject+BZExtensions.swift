//
//  NSObject+BZExtensions.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 20.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import Foundation

extension NSObject
{
    //MARK: Constants (Public)
    
    //MARK: Constants (Private)
    
    private struct AssociatedKeys
    {
        static var isFirstLoadKey = "isFirstLoadKey"
    }
    
    //MARK: Property (Public)
    
    //MARK: Property (Private)
    
    //MARK: Class property (Public)
    
    public var isFirstLoad: Bool?
    {
        get
        {
            let isFirstLoad = objc_getAssociatedObject(self, &AssociatedKeys.isFirstLoadKey) as? Bool
            if isFirstLoad == nil
            {
                objc_setAssociatedObject(self, AssociatedKeys.isFirstLoadKey, false, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return false
            }
            return isFirstLoad
        }
        set
        {
            if let newValue = newValue
            {
                objc_setAssociatedObject(self, AssociatedKeys.isFirstLoadKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    //MARK: Class property (Private)
    
    //MARK: Class Methods (Public)
    
    //MARK: Class Methods (Private)
    
    //MARK: Init & Dealloc
    
    //MARK: Notifications
    
    //MARK: Delegates()
    
    //MARK: Methods(Public)
    
    func methodGetRetainCount() -> Int!
    {
        return CFGetRetainCount(self)
    }
    
    //MARK: Methods(Private)
    
    //MARK: Standart methods
}




























