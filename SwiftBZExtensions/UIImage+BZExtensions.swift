//
//  UIImage+BZExtensions.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 21.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit

extension  UIImage
{
    //MARK: Constants (Public)
    
    //MARK: Constants (Private)
    
    private struct AssociatedKeys
    {
        static var theMapTableKey = "theMapTableKey"
    }
    
    private static var theImageMap: NSMapTable<NSString, UIImage> = NSMapTable(keyOptions: .strongMemory, valueOptions: .weakMemory)
    
    //MARK: Property (Public)
    
    //MARK: Property (Private)
    
    //MARK: Class property (Public)
    
    //MARK: Class property (Private)
    
    //MARK: Class Methods (Public)
    
    //MARK: Class Methods (Private)
    
    //MARK: Init & Dealloc
    
    //MARK: Notifications
    
    //MARK: Delegates()
    
    //MARK: Methods(Public)
    
    class func methodGetImage(name: String) -> UIImage?
    {
        var theFoundImage = UIImage.theImageMap.object(forKey: name as NSString)
        if let theFoundImage = theFoundImage
        {
            return theFoundImage
        }
        theFoundImage = methodPrivateGetImage(name: name)
        if let theFoundImage = theFoundImage
        {
            UIImage.theImageMap.setObject(theFoundImage, forKey: name as NSString)
        }
        return theFoundImage
    }
    
    //MARK: Methods(Private)
    
    class func methodPrivateGetImage(name: String) -> UIImage?
    {
        var theDevice:BZDevice? = nil
        var theFileName:String? = nil;
        //check for retina display
        if (UIScreen.main.isRetinaDisplay() == true)
        {
            theDevice = BZSwiftExtManager.methodGetDevice()
            switch (theDevice!)
            {
            case .Undefined:
                    theFileName = name + ".png";
            case .Iphone6_7_Plus:
                    theFileName = name + ".png";
            case .Iphone6_7:
                    theFileName = name + ".png";
            case .Iphone5:
                theFileName = name + ".png";
            case .Iphone4:
                theFileName = name + ".png";
            case .Ipad:
                theFileName = name + ".png";
            case .IpadPro:
                theFileName = name + ".png";
            }
        }
        else
        {
            theDevice = BZSwiftExtManager.methodGetDevice()
            if (theDevice! == .Iphone6_7_Plus)
            {
                theFileName = name + ".png";
            }
            else
            {
                theFileName = name + ".png";
            }
            // non-Retina display
        }
        if let theFileName = theFileName
        {
                    let theImage = UIImage.init(contentsOfFile: Bundle.main.path(forResource: theFileName, ofType: nil)!)
            return theImage
        }
        return nil

    }
    
    //MARK: Standart methods
}




























