//
//  MarkClass.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 19.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import Foundation

class MarkClass: NSObject
{
    //MARK: Constants (Public)
    
    //MARK: Constants (Private)
    
    //MARK: Property (Public)
    
    //MARK: Property (Private)
    
    //MARK: Class property (Public)
    
    //MARK: Class property (Private)
    
    //MARK: Class Methods (Public)
    
    //MARK: Class Methods (Private)
    
    //MARK: Init & Dealloc
    
    //MARK: Notifications
    
    //MARK: Delegates()
    
    //MARK: Methods(Public)
    
    //MARK: Methods(Private)
    
    //MARK: Standart methods
}
