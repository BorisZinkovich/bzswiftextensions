//
//  String+BZExtension.swift
//  SwiftBZExtensions
//
//  Created by User on 16.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import Foundation

extension String {
    private struct AssociatedKeys {
        static var theUndefinedKey = "isUndefined"
    }
    
    static var theArrayIndex : Int! = -1;
   
    private var isUndefined : Bool?
    {
        get
        {
            let theR = objc_getAssociatedObject(self, &AssociatedKeys.theUndefinedKey)
            return theR == nil ? false : theR as! Bool
        }
        set
        {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theUndefinedKey,
                    newValue as Bool?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
    
   public var theDeviceValue: Double?
    {
          get {
            let theStringsArray = self.components(separatedBy:" ")
            assert(theStringsArray.count == 6)
            if (String.theArrayIndex == -1)
            {
                let theDevice = BZSwiftExtManager.methodGetClosestDevice();
                switch (theDevice)
                {
                case .Iphone4:
                     String.theArrayIndex = 5
                case .Iphone5:
                     String.theArrayIndex = 4
                case .Iphone6_7:
                     String.theArrayIndex = 3
                case .Iphone6_7_Plus:
                     String.theArrayIndex = 2;
                case .Ipad:
                    String.theArrayIndex = 1;
                case .IpadPro:
                    String.theArrayIndex = 0;
                case .Undefined:
                    assert(false)
                }
            }
            return Double(theStringsArray[String.theArrayIndex])
        }
    }
    
    public static func saveDataFromHexString(string: String!) -> Data
    {
        var theString = string
        if string.characters.count % 2 != 0
        {
            var theRes = "f"
            theRes.append(string)
            theString = theRes
        }
        return String.dataFromHexString(string: theString)
    }
    
    public static func dataFromHexString(string: String!) -> Data
    {
        if string.characters.count % 2 != 0
        {
            assert(false)
        }
        var hex: String! = string
        var data = Data()
        while(hex.characters.count > 0)
        {
            let c: String = hex.substring(to: hex.index(hex.startIndex, offsetBy: 2))
            hex = hex.substring(from: hex.index(hex.startIndex, offsetBy: 2))
            var ch: UInt32 = 0
            Scanner(string: c).scanHexInt32(&ch)
            var char = UInt8(ch)
            data.append(&char, count: 1)
        }
        return data
    }
}




























