//
//  ViewController.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 13.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let tt = "12 10 8 8 7 8".theDeviceValue
        print(tt ?? 0)
        var theData = String.dataFromHexString(string: "ff1a")
        let ptr = UnsafeMutablePointer<UInt8>.allocate(capacity: theData.count)
        theData.copyBytes(to: ptr, count: theData.count)
        for i in 0..<theData.count
        {
            print(ptr[i])
        }

//        var theData = Data(count: 10)
//        BZSwiftExtManager.methodArrayFill(withData: &theData, withByte: 2)
//        var theData2 = Data(count: 4)
//        BZSwiftExtManager.methodArrayFill(withData: &theData2, withByte: 4)
//        BZSwiftExtManager.methodArrayCopy(srcBuffer: theData2, srcPos: 1, destData: &theData, destPos: 2, length: 4)
//        var ptr = UnsafeMutablePointer<UInt8>.allocate(capacity: theData.count)
//        theData.copyBytes(to: ptr, count: theData.count)
//        for i in 0..<theData.count
//        {
//            print(ptr[i])
//        }
        BZSwiftExtManager.methodPrintAllFonts()
        let theView = UIImageView()
        print(theView.methodGetRetainCount())
        self.view.addSubview(theView)
        print(theView.methodGetRetainCount())
        print(theView.theCenterX)
        theView.theWidth = 100
        theView.theHeight = 200
        theView.theCenterX = theView.superview!.theWidth / 2
        theView.theCenterY = theView.superview!.theHeight / 2
        theView.image = UIImage.methodGetImage(name: "image2")
//        theView.backgroundColor = UIColor.getColorFromHexString(hex: "ff46f4")
        theView.theLeftSeparatorView.theWidth = 7
        theView.theLeftSeparatorView.backgroundColor = UIColor.yellow
        theView.theTopSeparatorView.theHeight = 3
        theView.theTopSeparatorView.backgroundColor = UIColor.green
        
        let theHighlightButton = UIButton()
        self.view.addSubview(theHighlightButton)
        theHighlightButton.theWidth = 100
        theHighlightButton.theHeight = 100
        theHighlightButton.theCenterX = theHighlightButton.superview!.theWidth / 2
        theHighlightButton.theCenterY = theHighlightButton.superview!.theHeight / 2 + 150
        theHighlightButton.backgroundColor = UIColor.red
        theHighlightButton.theHighlightedBackgroundColor = UIColor.yellow
        theHighlightButton.addTarget(self, action:  #selector(ViewController.test), for: UIControlEvents.touchUpInside)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3)
        {
            theView.theWidth = 10
            theView.theMaxX = theView.superview!.theWidth - 10
        }
        
//        let theUnionFindManager = UnionFindManager(theArray: [1, 2, 7, 12, 9, 3, 8])
//        theUnionFindManager.methodUnion(first: 1, second: 7)
//        theUnionFindManager.methodUnion(first: 3, second: 8)
//        theUnionFindManager.methodUnion(first: 7, second: 9)
//        print(theUnionFindManager.methodFind(first: 1, second: 7))
//        print(theUnionFindManager.methodFind(first: 1, second: 3))
        
        
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func test() -> Void {
    }
}

