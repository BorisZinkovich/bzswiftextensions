//
//  UIButton+BZExtensions.swift
//  SwiftBZExtensions
//
//  Created by Boris Zinkovich on 20.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit

extension UIButton
{
    //MARK: Constants (Public)
    
    //MARK: Constants (Private)
    
    private struct AssociatedKeys
    {
        static var theHighlightedBackgroundColorKey = "theHighlightedBackgroundColorKey"
        static var theNormalBackgroundColorKey = "theNormalBackgroundColorKey"
        static var theIsCalledInsideKey = "theIsCalledInsideKey"
    }
    
    //MARK: Property (Public)
    
    public var theHighlightedBackgroundColor: UIColor?
    {
        get
        {
            let theColor = objc_getAssociatedObject(self, &AssociatedKeys.theHighlightedBackgroundColorKey) as? UIColor
            return theColor
        }
        set
        {
            
            if let newValue = newValue
            {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theHighlightedBackgroundColorKey,
                    newValue as UIColor,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
                self.theNormalBackgroundColor = self.backgroundColor
            }
        }
    }
    
    //MARK: Property (Private)
    
    private var theNormalBackgroundColor: UIColor?
    {
        get
        {
            let theColor = objc_getAssociatedObject(self, &AssociatedKeys.theNormalBackgroundColorKey) as? UIColor
            return theColor
        }
        set
        {
            if let newValue = newValue
            {
                objc_setAssociatedObject(
                    self,
                    &AssociatedKeys.theNormalBackgroundColorKey,
                    newValue as UIColor,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
    
    private var isCalledInside: Bool?
    {
        get
        {
            let isCalledInside = objc_getAssociatedObject(self, &AssociatedKeys.theIsCalledInsideKey) as? Bool
            if isFirstLoad == nil
            {
                objc_setAssociatedObject(self, AssociatedKeys.theIsCalledInsideKey, false, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return false
            }
            return isCalledInside
        }
        set
        {
            if let newValue = newValue
            {
                objc_setAssociatedObject(self, AssociatedKeys.theIsCalledInsideKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            }
        }
    }
    
    //MARK: Class property (Public)
    
    //MARK: Class property (Private)
    
    //MARK: Class Methods (Public)
    
    //MARK: Class Methods (Private)
    
    //MARK: Init & Dealloc
    
    open override class func initialize() {
        
        
        // make sure this isn't a subclass
        if self !== UIButton.self {
            return
        }
        
        //            // Accessor
        //            method_exchangeImplementations(
        //                class_getInstanceMethod(self, Selector("swizzledBZ_frame")),
        //                class_getInstanceMethod(self, Selector("frame"))
        //            )
        // One-param setter
        
        method_exchangeImplementations(
            class_getInstanceMethod(self, #selector(UIButton.swizzledBZSetHighligted(isHighlighted:))),
            class_getInstanceMethod(self, #selector(setter: UIButton.isHighlighted)))
    }
    
    //MARK: Notifications
    
    //MARK: Delegates()
    
    //MARK: Methods(Public)
    
    //MARK: Methods(Private)
    
    func swizzledBZSetHighligted(isHighlighted: Bool)
    {
        swizzledBZSetHighligted(isHighlighted: isHighlighted)
        if isHighlighted
        {
            if let theHighlightedColor = self.theHighlightedBackgroundColor
            {
                self.isCalledInside = true
                self.backgroundColor = theHighlightedColor
            }
        }
        else
        {
            if let theNormalBackgroundColor = self.theNormalBackgroundColor
            {
                self.isCalledInside = true
                self.backgroundColor = theNormalBackgroundColor
            }
        }
    }
    
    //MARK: Standart methods
}




























