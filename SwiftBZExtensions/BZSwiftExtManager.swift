//
//  BZSwiftExtManager.swift
//  SwiftBZExtensions
//
//  Created by User on 16.04.17.
//  Copyright © 2017 Boris Zinkovich. All rights reserved.
//

import UIKit

enum BZDevice {
    case Iphone4
    case Iphone5
    case Iphone6_7
    case Iphone6_7_Plus
    case Ipad
    case IpadPro
    case Undefined
}

func sfs(theSelector: Selector) -> String
{
    return NSStringFromSelector(theSelector)
}

class BZSwiftExtManager
{
    class func methodGetStatusBarHeight() -> Double
    {
        return Double(UIApplication.shared.statusBarFrame.size.height)
    }
    
    class func methodIsIOS8() -> Bool
    {
        if NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_8_0
        {
            return true
        }
        return false
    }
    
    class func methodArrayFill(withData:inout Data, withByte: UInt8)
    {
        withData.withUnsafeMutableBytes {(bytes: UnsafeMutablePointer<UInt8>)->Void in
            for i in 0..<withData.count
            {
                bytes[i] = withByte
            }
        }
    }
    
    class func methodArrayFill(sourceData:inout Data, srcPos:Int, destPos:Int, byte: UInt8)
    {
        sourceData.withUnsafeMutableBytes {(bytes: UnsafeMutablePointer<UInt8>)->Void in
            for i in srcPos...destPos
            {
                bytes[i] = byte
            }
        }
    }
    
    class func methodArrayCopy(srcBuffer:Data, srcPos:Int, destData:inout Data, destPos:Int, length:Int)
    {
        let ptr = UnsafeMutablePointer<UInt8>.allocate(capacity: srcBuffer.count)
        srcBuffer.copyBytes(to: ptr, count: srcBuffer.count)
        destData.withUnsafeMutableBytes {(bytes: UnsafeMutablePointer<UInt8>)->Void in
            var theStartIndex = srcPos
            for i in destPos..<destPos + length
            {
                bytes[i] = ptr[theStartIndex]
                theStartIndex += 1
            }
        }
    }
//
//    static float theTimeInterval = 0;
//    
//    +  (NSData * _Nonnull)methodArrayCopyWithSrcBuffer:(NSData * _Nonnull)theSdata withSrcPos:(int)theSrcPos withDestData:(NSData * _Nonnull)theData withDestPos:(int)theDestPos withLength:(int)theLength
//    {
//    Byte *theParsedBytePtr = theData.toMutableBytes;
//    Byte *theBufferBytePtr = theSdata.toMutableBytes;
//    int theStartIndex = theSrcPos;
//    //        System.arraycopy(buffer, index, parsed, parsedIndex, L);
//    for (int i = theDestPos; i < theDestPos + theLength; i++)
//    {
//    theParsedBytePtr[i] = theBufferBytePtr[theStartIndex];
//    theStartIndex++;
//    }
//    NSData *theResultData = [NSData dataWithBytes:theParsedBytePtr length:theData.length];
//    return theResultData;
//    }

    
    class func methodPrintAllFonts()
    {
        for theFamily in UIFont.familyNames
        {
            for theName in UIFont.fontNames(forFamilyName: theFamily)
            {
                print(theName)
            }
        }
    }
    
    class func methodGetClosestDevice() -> BZDevice
    {
        let theInterfaceOrientation = UIApplication.shared.statusBarOrientation
        let theSize = UIScreen.main.bounds.size
        var theSpecialMetrick : Double? = nil
        if (theInterfaceOrientation == UIInterfaceOrientation.portrait || theInterfaceOrientation == UIInterfaceOrientation.portraitUpsideDown)
        {
            theSpecialMetrick = Double(theSize.height)
        }
        else
        {
            theSpecialMetrick = Double(theSize.width)
        }
        var theValuesArray : Array<Double> = Array(arrayLiteral: 480, 568, 667, 736, 1024, 1366)
        var theFloorsArray = Array<Double>()
        for i in 0..<theValuesArray.count
        {
            var theValue : Double = theValuesArray[i]
            theValue = fabs(theValue - theSpecialMetrick!)
            theFloorsArray.append(theValue)
        }
        for _ in 0..<theFloorsArray.count
        {
            for j in 0..<theFloorsArray.count - 1
            {
                if theFloorsArray[j+1] < theFloorsArray[j]
                {
                    let theTemp = theFloorsArray[j+1]
                    theFloorsArray[j+1] = theFloorsArray[j]
                    theFloorsArray[j] = theTemp
                    let theTemp2 = theValuesArray[j+1]
                    theValuesArray[j+1] = theValuesArray[j]
                    theValuesArray[j] = theTemp2
                }
            }
        }
        theSpecialMetrick = theValuesArray[0]
        if theSpecialMetrick == 480
        {
            return BZDevice.Iphone4
        }
        if theSpecialMetrick == 568
        {
            return BZDevice.Iphone5
        }
        if theSpecialMetrick == 667
        {
            return BZDevice.Iphone6_7
        }
        if theSpecialMetrick == 736
        {
            return BZDevice.Iphone6_7_Plus;
        }
        if theSpecialMetrick == 1024
        {
            return BZDevice.Ipad;
        }
        if theSpecialMetrick == 1366
        {
            return BZDevice.IpadPro;
        }
        return BZDevice.Iphone4

    }
    
    class func methodGetDevice() -> BZDevice
    {
        let theDeviceOrientation = UIApplication.shared.statusBarOrientation
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad)
        {
            let theSize = UIScreen.main.bounds.size;
            var theSpecialMetric: Double? = nil;
            if (theDeviceOrientation == UIInterfaceOrientation.portrait || theDeviceOrientation == UIInterfaceOrientation.portraitUpsideDown)
            {
                theSpecialMetric = Double(theSize.height)
            }
            else
            {
                theSpecialMetric = Double(theSize.width)
            }
            if (theSpecialMetric == 1024)
            {
                return BZDevice.Ipad;
            }
            if (theSpecialMetric == 1366)
            {
                return BZDevice.IpadPro;
            }
            return BZDevice.Ipad;
        }
        else
        {
            let theSize = UIScreen.main.bounds.size;
            var theSpecialMetric: Double? = nil;
            if (theDeviceOrientation == UIInterfaceOrientation.portrait || theDeviceOrientation == UIInterfaceOrientation.portraitUpsideDown)
            {
                theSpecialMetric = Double(theSize.height)
            }
            else
            {
                theSpecialMetric = Double(theSize.width)
            }
            if (theSpecialMetric == 480)
            {
                return BZDevice.Iphone4;
            }
            if (theSpecialMetric == 568)
            {
                return BZDevice.Iphone5;
            }
            if (theSpecialMetric == 667)
            {
                return BZDevice.Iphone6_7;
            }
            if (theSpecialMetric == 736)
            {
                return BZDevice.Iphone6_7_Plus;
            }
        }
        return BZDevice.Undefined;
    }
}






























